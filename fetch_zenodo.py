import requests
import pandas as pd
from pprint import pprint

r = requests.get('https://zenodo.org/api/records/',
                 params={'q': 'creators.affiliation:sheffield', 'size': 100})

if r.status_code == 200:
    data = r.json()

    total = data['hits']['total']
    print('Found {} hits'.format(total))

    records = []
    for hit in data['hits']['hits']:
        md = hit['metadata']
        record_info = {
            'title': md['title'],
            'date': md['publication_date'],
            'type': md['resource_type']['type'],
            'license': md['license']['id'],
            'doi': md['doi'],
            'creators': '; '.join(['{name} ({affiliation})'.format(name=c['name'], affiliation=c.get('affiliation', 'none')) for c in md['creators']])
        }
        records.append(record_info)

    records = pd.DataFrame(records)

    records.to_csv('zenodo_sheffield.csv')

else:
    print('Error occurred: {}'.format(r.status_code))
